%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Politecnico di Milano - Corso di Studi in Ingegneria Informatica
%
% Meccanica (per Ingegneria Informatica) - R.Corradi, A.Facchinetti
%
% Scheda di approfondimento n.1 : LA CICLOIDE
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
close all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

R=1;                           % raggio del disco (m)
theta=0:480;                   % posizione angolare disco (deg)
theta=theta*pi/180;            % posizione angolare disco (rad)

xP=R*(theta-sin(theta));       % coordinate del punto P (m), al variare di theta
yP=R*(1-cos(theta));

theta=[0 30 120 180 330 360];  % considero set di N=6 posizioni angolari del disco (deg)
theta=theta*pi/180;
N=length(theta);
phi=0:359;                     % deg
phi=phi*pi/180;                % rad

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure

plot([-2 8],[0 0],'k')         % disegno guida rettilinea
hold on
plot(xP,yP),axis equal         % disegno cicloide

for ii=1:N                                 % disegno il disco nelle N posizioni considerate
    xQ=R*theta(ii);                        % posizione del centro Q (m)
    yQ=R;
    xP=R*(theta(ii)-sin(theta(ii)));       % posizione del punto P (m)
    yP=R*(1-cos(theta(ii)));
    x=xQ+cos(phi);                         % cordinate x,y di 360 punti equispaziati sulla circonferenza 
    y=yQ+sin(phi);
    plot(x,y,'r',xQ,yQ,'r+',xP,yP,'ro')    % disegno il disco e i punti Q e P
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

omega=0.1;                     % velocit� angolare del disco (rad/s)
omegap=0;                      % accelerazione angolare del disco (rad/s2)
t_max=ceil(4*pi/omega);        % definisco il tempo massimo corrispondente a 2 rotazioni complete del disco
t=0:.1:t_max;                  % vettore tempo (s)

vPx=omega*R*(1-cos(omega*t));  % velocit� del punto P - componenti x e y (m/s)
vPy=omega*R*sin(omega*t);

vP=sqrt(vPx.^2+vPy.^2);        % velocit� del punto P - modulo (m/s)
alpha=atan(vPy./vPx);          % angolo (rad) che definisce la direzione del vettore velocit�

aPx=omegap*R*(1-cos(omega*t))+omega^2*R*sin(omega*t);   % accelerazione del punto P - componenti x e y (m/s2)
aPy=omegap*R*sin(omega*t)+omega^2*R*cos(omega*t);

aP=sqrt(aPx.^2+aPy.^2);                                 % accelerazione del punto P - modulo (m/s2)

aPt=aPx.*cos(alpha)+aPy.*sin(alpha);                    % accelerazione del punto P - componenti t e n (m/s2)
aPn=aPx.*sin(alpha)-aPy.*cos(alpha);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure  % grafici velocit� del punto P in funzione di t (componente x, componente y, modulo)

subplot(311),plot(t,vPx),grid,ylabel('v_P_x [m/s]'),title('R = 1.0 m    omega = 0.1 rad/s')
subplot(312),plot(t,vPy),grid,ylabel('v_P_y [m/s]')
subplot(313),plot(t,vP),grid,ylabel('v_P [m/s]'),xlabel('tempo [s]')

figure  % grafici accelerazione del punto P in funzione di t (componente x, componente y, modulo)

subplot(311),plot(t,aPx),grid,ylabel('a_P_x [m/s^2]'),title('R = 1.0 m    omega = 0.1 rad/s')
subplot(312),plot(t,aPy),grid,ylabel('a_P_y [m/s^2]')
subplot(313),plot(t,aP),grid,ylabel('a_P [m/s^2]'),xlabel('tempo [s]')

figure  % grafici accelerazione del punto P in funzione di t (componente t, componente n, modulo)

subplot(311),plot(t,aPt),grid,ylabel('a_P_t [m/s^2]'),title('R = 1.0 m    omega = 0.1 rad/s')
subplot(312),plot(t,aPn),grid,ylabel('a_P_n [m/s^2]')
subplot(313),plot(t,aP),grid,ylabel('a_P [m/s^2]'),xlabel('tempo [s]')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    










