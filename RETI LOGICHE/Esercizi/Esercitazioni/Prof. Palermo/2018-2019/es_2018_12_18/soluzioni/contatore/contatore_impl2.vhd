library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity contatore_impl2 is
  port(
      clk:  in std_logic;
      rst:  in std_logic;
      cont: out std_logic_vector(2 downto 0);
      slowclock: out std_logic

  );
end contatore_impl2;

architecture impl of contatore_impl2 is
  signal cont_next_state, cont_current_state: std_logic_vector(2 downto 0);
  signal slowclk_next_state, slowclk_current_state: std_logic;
begin

  state_reg: process(clk, rst)
  begin
    if rst='1' then
      cont_current_state <= (others => '0');
      slowclk_current_state <= '0';
    elsif rising_edge(clk) then
      cont_current_state <= cont_next_state;
      slowclk_current_state <= slowclk_next_state;
    end if;
  end process;
  
  next_state_proc : process(cont_current_state)
  begin
    if cont_current_state = "100" then
      cont_next_state <= "000";
      slowclk_next_state <= not slowclk_current_state;
    else
      cont_next_state <= std_logic_vector(unsigned(cont_current_state)+1);
      slowclk_next_state <= slowclk_current_state;
    end if;
  end process;

  cont <= cont_current_state;
  slowclock <= slowclk_current_state;

end impl;
    

